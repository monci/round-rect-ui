The Round Rect UI is an editor that I created at Unity to make it easier to create and modify a solid color image that was originally only angled in shape, to be rounded / has an adjustable radius.

I have created 4 types that can be used:
- Full Round Rect (Make border radius for all corners in full, taking into account the length and width).
- One Edge Round Rect (Create a custom border radius only for the top, bottom, right or left side.
- Uniform Round Rect (Create a custom uniform border radius for all corners).
- Free Round Rect (Create a custom radius border freely for each corner).

Besides that, I also added width settings (in this case, the width of the outline / stroke) and the fall of distance.